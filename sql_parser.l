%{
#include <stdio.h>
#include <stdlib.h>
#include "sql_parser.tab.h"
#include <string.h>
%}

%%

"CREATE"      { return CREATE; }
"TABLE"       { return TABLE; }
"INTEGER"     {return INTEGER;}
"TEXT"        {return TEXT;}
"REAL"        {return REAL;}
[a-zA-Z_][a-zA-Z0-9_]* {
    yylval.str = strdup(yytext);
    return IDENTIFIER;
}
\"([^\\\n]|(\\.))*?\"   { yylval.str = strdup(yytext); return TEXT_LITERAL; }
[0-9]+                  { yylval.str = strdup(yytext); return INTEGER_LITERAL; }
[0-9]+"."[0-9]*         { yylval.str = strdup(yytext); return REAL_LITERAL; }

[ \t\r]+ {
    // Ignore espaços em branco, tabulações e retornos
}

\n            { return EOL; }
.             { return yytext[0]; }
%%

int yywrap() {
    return 1;
}
