# Trabalho Compiladores

Este é o repositório para o trabalho da disciplina de Compiladores. O objetivo deste trabalho é desenvolver um programa que realize a análise léxica e sintática de um CREATE TABLE em SQLite. O programa recebe como entrada um código fonte escrito nessa linguagem e produz como saída um MODEL em C das colunas do CREATE TABLE.
