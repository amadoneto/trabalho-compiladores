%{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void yyerror(const char* message) {
    fprintf(stderr, "Error: %s\n", message);
    exit(1);
}

extern int yylex();
extern int yyparse();
extern FILE* yyin;
extern char* yytext;

char* id_seq_list = NULL;

void appendColumnId(char** list, char* identifier, char* type) {
    if (*list == NULL) {
        *list = strdup(identifier);
        size_t idLen = strlen(identifier);
        size_t idType = strlen(type);
        *list = realloc(*list, idLen + idType + 3);
        sprintf(*list, "%s %s", identifier, type);
    } else {
        size_t listLen = strlen(*list);
        size_t idLen = strlen(identifier);
        size_t idType = strlen(type);
        *list = realloc(*list, listLen + idLen + idType + 4);
        sprintf(*list, "%s, %s %s", *list, identifier, type);
    }
}

void generateCSharpModel(const char* tableName, const char* columns) {
    FILE* modelFile = fopen("Model.cs", "w");
    if (modelFile == NULL) {
        fprintf(stderr, "Error: Unable to create model file.\n");
        exit(1);
    }

    fprintf(modelFile, "public class %sModel\n", tableName);
    fprintf(modelFile, "{\n");

    char* column = strtok(columns, ", ");
    while (column != NULL) {
        char* type = strtok(NULL, ", ");
        if (type == NULL) {
            fprintf(stderr, "Error: Invalid column format.\n");
            exit(1);
        }
        fprintf(modelFile, "    public %s %s { get; set; }\n", type, column);
        column = strtok(NULL, ", ");
    }

    fprintf(modelFile, "}\n");
    fclose(modelFile);

    printf("Model file generated: Model.cs\n");
}

%}

%union {
    char* str;
}

%token <str> IDENTIFIER
%token CREATE
%token TABLE
%token '(' ')'
%token ','
%token EOL
%token <str> INTEGER TEXT REAL BLOB NUMERIC PRIMARY_KEY TEXT_LITERAL INTEGER_LITERAL REAL_LITERAL

%type <str> program idType column_list

%%

program: table_declaration EOL
;
table_declaration: CREATE TABLE IDENTIFIER '('column_list IDENTIFIER idType')' {
    if (id_seq_list == NULL) {
        printf("CREATE TABLE %s( %s %s );\n", $3, $6, $7);
        printf("AQUI NO IF ");
        //generateCSharpModel($3, $6);
    } else {
        printf("CREATE TABLE %s(%s %s %s );\n", $3, id_seq_list, $6, $7);
        printf("id_seq_list TABLE %s\n", id_seq_list);
        generateCSharpModel($3, id_seq_list);
        free(id_seq_list);
    }
}
;
column_list: 
| column_list IDENTIFIER idType ',' {appendColumnId(&id_seq_list, $2, $3);}
;
idType: INTEGER {$$ = "int";}
| TEXT {$$ = "string";}
| REAL {$$ = "float";}
| BLOB {$$ = "byte[]";}
| NUMERIC {$$ = "decimal";}
| PRIMARY_KEY {$$ = "int";}
| IDENTIFIER {$$ = $1;} // For custom types, assumes IDENTIFIER is the type name
;

%%

int main() {
    yyparse();
    printf("Parsing completed.\n");
    return 0;
}
